const cacheName = 'dolores-workout-cache-v26';

const urlsToCache = [
    './css/bulma.css',
    './css/dolores-bulma-dark.css',
    './css/dolores-bulma-light.css',
    './css/dolores-font-codes.css',
    './css/dolores-font-embedded.css',
    './css/dolores-font-ie7-codes.css',
    './css/dolores-font-ie7.css',
    './css/dolores-font.css',
    './css/dolores.css',
    './css/font.css',
    './css/stats.css',
    './font/config.json',
    './font/dolores-font.eot',
    './font/dolores-font.svg',
    './font/dolores-font.ttf',
    './font/dolores-font.woff',
    './font/dolores-font.woff2',
    './font/fontello-config.json',
    './img/logo.png',
    './index.html',
    './js/calendar.js',
    './js/dbinit.js',
    './js/dolores.js',
    './js/nav.js',
    './js/settings.js',
    './js/stats.js',
    './manifest.json',
    './sound/beep-half.ogg',
    './sound/beep-workout.ogg',
    './sound/beep-rest.ogg',
    './sound/beep-workout-1.ogg',
    './sound/beep-workout-2.ogg',
    './sound/beep-workout-3.ogg',
    './sound/beep-workout-4.ogg',
    './sound/beep-workout-5.ogg',
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(cacheName)
            .then((cache) => {
                console.log('Opened cache');
                return Promise.all(
                    urlsToCache.map(url => {
                        return fetch(url).then(response => {
                            if (!response.ok) {
                                throw new Error('Network response was not ok');
                            }
                            return cache.put(url, response);
                        }).catch(error => {
                            console.error(`Failed to fetch and cache ${url}:`, error);
                        });
                    })
                );
            })
    );
    self.skipWaiting();
});

self.addEventListener('activate', (event) => {
    const cacheWhitelist = [cacheName];
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames.map((cacheName) => {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                    return null;
                })
            );
        })
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.open(cacheName).then((cache) => {
            return fetch(event.request).then((response) => {
                cache.put(event.request, response.clone());
                return response;
            }).catch(() => {
                return caches.match(event.request);
            });
        })
    );
});

function checkForUpdate() {
    self.registration.update();
}

self.addEventListener('message', (event) => {
    if (event.data.action === 'checkForUpdate') {
        checkForUpdate();
    } else if (event.data.action === 'skipWaiting') {
        self.skipWaiting();
    }
});
