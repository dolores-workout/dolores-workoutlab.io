// Open a new database connection
let request = window.indexedDB.open("WorkoutDB", 1);

request.onerror = function(event) {
  console.log("Error opening DB", event);
};

request.onupgradeneeded = function(event) {
  window.db = event.target.result;

  if (!window.db.objectStoreNames.contains('workouts')) {
    let objectStore = window.db.createObjectStore("workouts", { keyPath: "date" });
    objectStore.createIndex("date", "date", { unique: false });
  }

  if (!window.db.objectStoreNames.contains('values')) {
    let valuesStore = window.db.createObjectStore("values");
  }

  if (!window.db.objectStoreNames.contains('settings')) {
    window.db.createObjectStore("settings");
  }
  
  console.log("Success upgrading DB");
  document.dispatchEvent(new Event('dbReady')); // Move this line here
  console.log('dbReady sent');
};

request.onsuccess = function(event) {
  window.db = event.target.result;
  document.dispatchEvent(new Event('dbReady')); // Move this line here
  console.log('%cDB Init OK', 'color: green;');
};

