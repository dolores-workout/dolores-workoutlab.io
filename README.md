# Dolores Workout

A simple but powerful and complete workout timer with statistics.

This is a [PWA](https://en.wikipedia.org/wiki/Progressive_web_app), simply visit [Dolores Workout](https://dolores-workout.gitlab.io/) with your - both desktop or mobile -  browser and use "Install Dolores Workout" to have it available even when offline.

![Dolores](https://mobilohm.gitlab.io/img/shots/dolores.png)

If you find one of the [MobilOhm](https://mobilohm.gitlab.io/) apps helpful and would like to support its development, consider making a contribution through [Ko-fi](https://ko-fi.com/yphil) or [Liberapay](https://liberapay.com/yPhil/).
